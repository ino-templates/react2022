# React Boilerplate 2022

## Env

- Yarn
- TypeScript
- React
- Vite
- ESLint
- Prettier

## Setup

### 1. Create React Project with Vite

```shell-session
$ yarn create vite . --template react-ts
$ yarn
$ yarn dev
```

### 2. Setup ESLint

```shell-session
# below 2 commands does not create eslint config file
$ yarn add eslint --dev
$ yarn init @eslint/config
# type this
$ yarn create @eslint/config
# answer ...
$ yarn add --dev eslint-plugin-react@latest @typescript-eslint/eslint-plugin@latest eslint-config-standard@latest eslint@latest eslint-plugin-import@latest eslint-plugin-n@latest eslint-plugin-promise@latest @typescript-eslint/parser@lates
```

### 3. Setup Prettier

```shell-session
$ yarn add --dev prettier
$ echo {} > .prettierrc.json
$ echo "dist/**/*.js" > .prettierignore
$ yarn add --dev eslint-config-prettier # avoid conflict eslint and prettier
$ vim .eslintrc.json
# add "prettier" to extends (must be the bottom)
```
